package com.company;

public enum Genre {
    Country,
    Folk,
    Blues,
    Jazz,
    Rock,
    Pop,
    Rap,
    Romances;
}
