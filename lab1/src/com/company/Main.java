package com.company;

import java.util.ArrayList;
import java.lang.String;
public class Main {

        public static void main(String[] args) {
            ArrayList<Songs> list = new ArrayList<Songs>();
            list.add(new Songs("Louis Armstrong", "September 1, 1967", Genre.Jazz, "What a Wonderful World", 2.21, 11.97, "Songs"));
            list.add(new Songs("Muddy Waters", "June 1, 1955", Genre.Blues, "Mannish Boy", 2.55, 12.00, "Songs"));
            list.add(new Songs("Johnny Cash", "May 1, 1956", Genre.Country, "I Walk the Line", 2.45, 33.99, "Songs"));
            list.add(new Songs("Europe", "February 14, 1986", Genre.Rock, "The Final Countdown", 5.09, 3.85, "Songs"));
            list.add(new Songs("Volodymyr Ivasyuk", "September 1, 1970", Genre.Folk, "Chervona Ruta", 2.56, 0.99, "Songs"));
            list.add(new Songs("the Beach Boys", "July 18, 1966", Genre.Romances, "God Only Knows", 2.55, 1.29, "Songs"));
            list.add(new Songs("Eminem", "October 28, 2002", Genre.Rap, "Lose Yourself", 5.20, 15.25, "Songs"));
            list.add(new Songs("Rick Astley", "July 27, 1987", Genre.Pop, "Never Gonna Give You Up", 3.35, 40.99, "Songs"));

            System.out.println("\nAllList: ");
            list.forEach(
                    (el) -> {
                        System.out.println(el.author+' '+el.release_date+' '+
                                el.name+' '+el.genre+' '+el.duration+' '+el.price+' '+el.category);
                    }
            );

            Service_manager Manager = new Service_manager();

            System.out.println("\nsortByDuration: ");
            if (Manager.sortByDuration(list.get(0), list.get(1)) > 0)
            {
                System.out.println(list.get(0).name+" duration is bigger than "+list.get(1).name+" duration");
            }
            if (Manager.sortByDuration(list.get(0), list.get(1)) < 0)
            {
                System.out.println(list.get(1).name+" duration is bigger than "+list.get(0).name+" duration");
            }
            if (Manager.sortByDuration(list.get(0), list.get(1)) == 0)
            {
                System.out.println(list.get(0).name+" duration is equal as "+list.get(1).name+" duration");
            }

            System.out.println("\nsortByGenre: ");
            ArrayList<Songs> list2 = Manager.SortSongByGenre(list, Genre.Rock);
            for(Songs song : list2) {
                System.out.println(song.author+' '+song.release_date+' '+
                        song.genre+' '+song.name+' '+song.duration+' '+song.price+' '+song.category);

                System.out.println("\nsortByPrice: ");
                if (Manager.sortByDuration(list.get(2), list.get(3)) > 0)
                {
                    System.out.println(list.get(2).name+" actual price is lower than "
                            +list.get(3).name+" recommended price");
                }
                if (Manager.sortByDuration(list.get(2), list.get(3)) < 0)
                {
                    System.out.println(list.get(3).name+" actual price is higher than "
                            +list.get(2).name+" recommended price");
                }
                if (Manager.sortByDuration(list.get(2), list.get(3)) == 0)
                {
                    System.out.println(list.get(2).name+" actual price is equal as "
                            +list.get(3).name+" recommended price");
                }

                System.out.println("\nSortByPrice reverse: ");
                Manager.sortByAuthor(list, "is");
            }
            ArrayList<Integer> numbers = new ArrayList<Integer>();
            numbers.add(41);
            numbers.add(34);
            numbers.add(15);
            numbers.add(12);
            numbers.forEach(System.out::println);
    }
}

