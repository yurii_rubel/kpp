package com.company;

import java.lang.String;

public abstract class Music_services {
    public String name;
    public double duration;
    public double price;
    public String category;

    public String getName() {

        return name;
    }

    public double getDuration() {

        return duration;
    }

    public double getPrice() {

        return price;
    }

    public String getCategory() {

        return category;
    }

}
