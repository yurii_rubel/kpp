package com.company;

import java.util.*;
import java.util.Comparator;
import java.util.List;

 public class Service_manager {
     private class KomparatorI implements Comparator<Songs> {
         public int compare(Songs a1, Songs a2) {

             return Double.compare(a1.duration, a2.duration);
         }
     }

     public static class KomparatorII implements Comparator<Songs> {
         public int compare(Songs o1, Songs o2) {

             return Double.compare(o1.price, o2.price);
         }
     }

     public double sortByDuration(Songs firsongs, Songs secsong) {
         KomparatorI komparator = new KomparatorI();
         double compare = komparator.compare(firsongs, secsong);
         return compare;
     }

     public double sortByPrice(Songs Iprice, Songs IIprice) {
         KomparatorII komparator = new KomparatorII();
         double compare = komparator.compare(Iprice, IIprice);
         return compare;
     }

     public ArrayList<Songs> SortSongByGenre(ArrayList<Songs> List, Genre genre) {
         ArrayList<Songs> List2 = new ArrayList<Songs>();
         for (Songs song : List) {
             if (song.genre == genre) {
                 List2.add(song);
             }
         }
         return List2;

     }

     public void sortByAuthor(List<Songs> songsList, String author) {
         songsList.forEach(
                 (songs) -> {
                     if (!songs.author.equals(author)) {
                         return;
                     }
                     System.out.println(songs.release_date);
                 }
         );
     }
 }
