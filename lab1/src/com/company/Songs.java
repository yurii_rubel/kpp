package com.company;

public class Songs extends Music_services {
    public String author;
    public String release_date;
    public Genre genre;

    public Songs(String author, String release_date, Genre genre, String name,
                 double duration, double price, String category){
        this.author = author;
        this.release_date = release_date;
        this.genre = genre;
        this.name = name;
        this.duration = duration;
        this.price = price;
        this.category = category;
    }
}
