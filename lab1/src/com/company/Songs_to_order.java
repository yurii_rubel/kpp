package com.company;

public class Songs_to_order extends Music_services {
    public String type;

    public Songs_to_order(String type, String name, double duration, double price, String category){
        this.type = type;
        this.name = name;
        this.duration = duration;
        this.price = price;
        this.category = category;
    }
}
