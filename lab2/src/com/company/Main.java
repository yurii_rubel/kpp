package com.company;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.lang.String;

    public class Main {

        public static void main(String[] args) {

            Newspapers[] newNewspapers = new Newspapers[10];
            newNewspapers[0] = new Newspapers("New York Times", 1999, 1500, "weekly", 1);
            newNewspapers[1] = new Newspapers("The Times", 2000, 1000, "monthly", 2);
            newNewspapers[2] = new Newspapers("The City", 1995, 1000, "weekly", 3);
            newNewspapers[3] = new Newspapers("Morning", 2000, 1200, "monthly", 4);
            newNewspapers[4] = new Newspapers("The Washington Post", 1995, 1500, "weekly", 5);
            newNewspapers[5] = new Newspapers("The Independent", 2000, 1200, "monthly", 6);
            newNewspapers[6] = new Newspapers("The Daily Telegraph", 1995, 150, "weekly", 7);
            newNewspapers[7] = new Newspapers("Positive News", 2000, 1250, "monthly", 8);
            newNewspapers[8] = new Newspapers("People", 1995, 800, "weekly", 9);
            newNewspapers[9] = new Newspapers("Daily Mail", 1995, 800, "weekly", 10);

            //creating a map, entering data and displaying it on the screen
            HashMap<Integer, String> myHashMap = new HashMap<Integer, String>();//key (publisher), value - name
            HashMap<Integer, String> myNewHashMap = new HashMap<Integer, String>();//key (publisher), value - name

            for (int i = 0; i < 10; i++)
                myHashMap.put(newNewspapers[i].edition, newNewspapers[i].name);

            // creating a menu and displaying it on the screen
            while (true) {
                System.out.println("\n\n1 - Print map\n2 - Removing\n3 - Frequency response\n4 - Swapping\n" +
                        "5 - Read from file 1\n6 - Read from fle 2\n7 - Sorting by name reversed ");
                System.out.println("Enter your choice:");
                Scanner scan = new Scanner(System.in);
                String answ = scan.nextLine();

                // Standard
                if (Integer.parseInt(answ) == 1) {
                    System.out.println("Standard:\n");
                    for (Map.Entry<Integer, String> entry : myHashMap.entrySet())
                        System.out.println(entry.getKey() + " - " + entry.getValue());
                }

                // Removing
                else if (Integer.parseInt(answ) == 2) {
                    System.out.println("Removing:\n");
                    for (int i = 0; i < 3; i++) {
                        if (newNewspapers[i].printing < 900) {
                            String del = newNewspapers[i].name;
                            for (Map.Entry<Integer, String> entry : myHashMap.entrySet()) {
                                if (del.equals(entry.getValue()))
                                    myHashMap.remove(entry.getKey());
                            }

                        }
                    }
                    for (Map.Entry<Integer, String> entry : myHashMap.entrySet())
                        System.out.println(entry.getKey() + " - " + entry.getValue());
                }

                // Frequency response
                else if (Integer.parseInt(answ) == 3) {
                    int k = 10;
                    int[] arr = new int[k];
                    int[] arr2 = new int[k];
                    for (int i = 0; i < k; i++) {
                        arr[i] = 0;
                        arr2[i] = 0;
                    }
                    arr[0] = newNewspapers[0].printing;
                    arr2[0] = 1;
                    for (int i = 1; i < k; i++) {
                        for (int index = 0; index < k; index++) {
                            if (newNewspapers[i].printing == arr[index]) {
                                arr2[index] += 1;
                                break;
                            } else {
                                index++;
                                if (index < k && arr2[index] == 0) {
                                    arr[index] = newNewspapers[i].printing;
                                    arr2[index] += 1;
                                    index--;
                                    break;
                                } else index--;
                            }

                        }

                    }

                    for (int i = 0; i < k; i++) {
                        if (arr2[i] != 0)
                            System.out.println(arr[i] + " - " + arr2[i]);
                    }
                }

                // Sorting by key for swapping and swapping
                else if (Integer.parseInt(answ) == 4) {
                    System.out.println("Sorting by key for swapping:\n");
                    myHashMap.entrySet().stream()
                            .sorted(Map.Entry.<Integer, String>comparingByKey())
                            .forEach(System.out::println);
                    for (int i = 0; i < 9; i++) {
                        if ((float) i % 2 == 0) {
                            int kk = i++;
                            String temp = newNewspapers[i].name;
                            newNewspapers[i].name = newNewspapers[kk].name;
                            newNewspapers[kk].name = temp;
                        }
                    }

                    HashMap<Integer, String> myHashMap2 = new HashMap<Integer, String>();

                    for (int i = 0; i < 10; i++)
                        myHashMap2.put(newNewspapers[i].edition, newNewspapers[i].name);
                    System.out.println("Swapping:\n");
                    for (Map.Entry<Integer, String> entry : myHashMap2.entrySet())
                        System.out.println(entry.getKey() + " - " + entry.getValue());
                }

                // Read from file 1
                else if (Integer.parseInt(answ) == 5) {
                    try {
                        File file1 = new File("C:/Project`s/KPP/lab2/lab/note1.txt");
                        FileReader fr1 = new FileReader(file1);
                        BufferedReader reader1 = new BufferedReader(fr1);
                        String line1 = reader1.readLine();
                        System.out.println("Read from file 1:\n");
                        while (line1 != null) {
                            System.out.println(line1);
                            String[] subStr1;
                            String delimeter1 = " ";
                            subStr1 = line1.split(delimeter1);
                            line1 = reader1.readLine();
                            myNewHashMap.put(Integer.parseInt(subStr1[4]), subStr1[0]);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // Read from file 2
                else if (Integer.parseInt(answ) == 6) {
                    try {
                        File file2 = new File("C:/Project`s/KPP/lab2/lab/note2.txt");
                        FileReader fr2 = new FileReader(file2);
                        BufferedReader reader2 = new BufferedReader(fr2);
                        String line2 = reader2.readLine();
                        System.out.println("Read from file 2:\n");
                        while (line2 != null) {
                            System.out.println(line2);
                            String[] subStr2;
                            String delimeter2 = " ";
                            subStr2 = line2.split(delimeter2);
                            line2 = reader2.readLine();
                            myNewHashMap.put(Integer.parseInt(subStr2[4]), subStr2[0]);

                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // Create one map from two and sorting by name reversed
                else if (Integer.parseInt(answ) == 7) {
                    System.out.println("Create one map from two and sorting by name reversed:\n");
                    myNewHashMap.entrySet().stream()
                            .sorted(Map.Entry.<Integer, String>comparingByValue().reversed())
                            .forEach(System.out::println);
                }
            }
        }
    }