package com.company;

import java.lang.String;

public class Newspapers {
    public String name;
    public int year;
    public int printing;
    public String frequency;
    public int edition;

    public Newspapers(String s_name, int s_year, int s_printing, String s_frequency, int s_edition) {
        name = s_name;
        year = s_year;
        printing = s_printing;
        edition = s_edition;
        frequency = s_frequency;
    }

}