package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextHelper {
    private String text;
    private List<String> sentences;
    private List<String> oldSentences;
    private List<String> modifySentences;
    public TextHelper(String text)
    {
        this.text = text;
        sentences = new ArrayList<String>();
        modifySentences = new ArrayList<String>();
        oldSentences = new ArrayList<String>();
        this.fillSentences();
    }
    public void fillSentences()
    {
        Matcher matcher = Pattern.compile("([^.!?]+[.!?])").matcher(text);
        while (matcher.find())
        {
            String sentence = matcher.group(1);
            sentence = sentence.replaceFirst(".$","");
            sentence = sentence.trim();
            sentences.add(sentence);
        }
    }
    private void modifySentences()
    {
        for (int i=0; i<sentences.size(); i++) {
            List<String> words = Arrays.asList(sentences.get(i).split("\\s+").clone());

            int indexLongest = findLongestWordIndex(words);
            int indexVowel = findFirstVowelWordIndex(words);
            String longestWord = words.get(indexLongest);

            StringBuilder stringBuilderOld = new StringBuilder("");
            words.forEach((s) -> stringBuilderOld.append(s + " "));
            oldSentences.add(stringBuilderOld.toString());

            if( indexVowel != -1) {
                String vowelWord = words.get(indexVowel);
                words.set(indexVowel, longestWord);
                words.set(indexLongest, vowelWord);
            }

            StringBuilder stringBuilderModify = new StringBuilder("");
            words.forEach((s) -> stringBuilderModify.append(s + " "));
            modifySentences.add(stringBuilderModify.toString());
        }
    }
    public int findLongestWordIndex(List<String> words)
    {
        int indexLongest = 0;
        String longestWord = words.get(indexLongest);
        for( int k=1; k<words.size(); k++)
        {
            if( words.get(k).length() > longestWord.length() )
            {
                longestWord = words.get(k);
                indexLongest = k;
            }
        }
        return indexLongest;
    }
    public int findFirstVowelWordIndex(List<String> words)
    {
        int indexVowel = -1;
        for (int j=0; j<words.size(); j++) {
            Matcher matcher = Pattern.compile("^[euoia]", Pattern.CASE_INSENSITIVE).matcher(words.get(j));
            if(matcher.find())
            {
                indexVowel = j;
                break;
            }
        }
        return indexVowel;
    }
    public void showOldAndModify()
    {
        this.modifySentences();
        for( int i=0; i<modifySentences.size(); i++)
        {
            System.out.println("Sentence #" + (i+1));
            System.out.println(oldSentences.get(i));
            System.out.println("    " + modifySentences.get(i));
        }
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append("We have such text:\n" + text + "\n");
        sentences.forEach((i) -> sb.append(i+"\n"));
        return sb.toString();
    }
}
