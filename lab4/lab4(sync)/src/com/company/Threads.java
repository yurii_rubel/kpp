package com.company;

import java.lang.*;

class Threads implements Runnable {
    public Thread t;
    public String threadName;

     Threads (String name ) {
        threadName = name;
        System.out.println("Creating " + threadName);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void run() {
        System.out.println("Running " + threadName);
        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Thread: " + threadName + ", " + i);
                // Let the thread sleep for a while.
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread " + threadName + " interrupted.");
        }
        System.out.println("Thread " + threadName + " exiting.");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    static class Threads1{
        synchronized void printTable(){//synchronized method
            for(int i=1;i<=5;i++){
                System.out.println("Threads are synchronized");
                try{
                    Thread.sleep(400);
                }catch(Exception e){System.out.println(e);}
            }
        }
    }

    public Threads() {
        Threads R1 = new Threads( "Thread-1");
        R1.start();

        Threads R2 = new Threads( "Thread-2");
        R2.start();

        Threads1 R3 = new Threads1();
        R3.printTable();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}